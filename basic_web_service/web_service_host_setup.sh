#!/bin/bash - 
#===============================================================================
#
#          FILE: web_install.sh
# 
#         USAGE: ./web_install.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 26/07/16 10:43
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

useradd -m -G wheel,users admin
echo "admin ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
mkdir /home/admin/.ssh
cat > /home/admin/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCu25cbwUSf2jqveyo/jOle1U2c4V0VXgKYMS9G/374TLxcslzPp2rvPSXYiQIibVSqBv/thvxs8iRm9uLNtd3dwD8Npb/RfXd/I0upoMdMSj/1cDQoY/Rype/JLlaBCdv9UIZeJaur/Ddr0ZdBS7ftMmrOow3A4Tv6cejC+D/wMHr2HVi7lb0zS8vewhhCQSjbx6t+2MxU8c7xfEy944abc6AIHIixJjVo0ETivC9+GPQopF7fWFfrUuErf+1CRerTX3MvsSWSVvdzfvjqnDkW7BAQUsYaWdh6ladXBkxua32UCiqXNwusmXzyeSCVNh8Zt+yi1yT3ZQvHZlW4YWehMQGKxCSLJkkPCcITCkX4l02cs2OMo6Fd5bwggdoXRv1BY9o2/3FXHdYry+oampOyORUYijo6hUs7BbcEUlKMp+LCdr+vOwAjlvKZ5NfgfOxUVAvwcO89fSteYSmd5i6+VNVjBpytXNshMLZA9XZN6fBuYYsL4rf6IWvbWbsrgRzcmas4lcR+UB4SkPTVPAqIiQ0sYENwT03g2wXDHjEdLEVjDnDi9ib8hnl/J1ZeAbVFjFKN8hvP6VCe1tBoWeHmxoDKRsF85dCYpVCaqTi0B4Mbs78Ew0w9bh7GYVSgRkJahXDu9qUOAyuuE0WQRgDvCtduIygFpHNdiX3FxrK7IQ== nasp18_admin
EOF
chown -R admin:admin ~admin/.ssh

yum install -y @core
yum install -y @base
yum install -y epel-release 
yum install -y nginx 
yum install -y mariadb-server 
yum install -y mariadb 
yum install -y php 
yum install -y php-mysql 
yum install -y php-fpm 
yum install -y wget 
yum -y update 

systemctl start firewalld
systemctl enable firewalld

declare zone=public
declare local_port1=22
declare local_port2=80
declare local_port3=443
sudo firewall-cmd --zone=$zone --add-port=$local_port1/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$local_port2/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$local_port3/tcp --permanent

systemctl restart firewalld

systemctl start nginx
systemctl enable nginx

systemctl start mariadb
mysql_secure_installation
systemctl enable mariadb


sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php.ini
sed -i "s/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm\/php-fpm.sock/" /etc/php-fpm.d/www.conf
sed -i "s/;listen.owner = nobody/listen.owner = nobody/" /etc/php-fpm.d/www.conf
sed -i "s/;listen.group = nobody/listen.group = nobody/" /etc/php-fpm.d/www.conf
sed -i "s/user = apache/user = nginx/" /etc/php-fpm.d/www.conf
sed -i "s/group = apache/group = nginx/" /etc/php-fpm.d/www.conf

systemctl start php-fpm
systemctl enable php-fpm
cat > /etc/nginx/nginx.conf << EOF
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
        index index.php index.html index.htm;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
        location ~ \.php\$ {
            try_files \$uri =404;
            fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
            include fastcgi_params;
        }
    }
}
EOF

echo "<?php phpinfo(); ?>" > /usr/share/nginx/html/info.php

systemctl restart nginx
cat > wordpress_setup.sql <<EOF
    CREATE DATABASE wordpress;
    CREATE USER wordpress_user@localhost IDENTIFIED BY 'nasp18';
    GRANT ALL PRIVILEGES ON wordpress.* TO wordpress_user@localhost;
    FLUSH PRIVILEGES;
EOF

mysql -u root --password=nasp18 < wordpress_setup.sql

wget http://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
cp wordpress/wp-config-sample.php wordpress/wp-config.php
sed -i "s/define('DB_NAME', 'database_name_here');/define('DB_NAME', 'wordpress');/" ./wordpress/wp-config.php
sed -i "s/define('DB_USER', 'username_here');/define('DB_USER', 'wordpress_user');/" ./wordpress/wp-config.php
sed -i "s/define('DB_PASSWORD', 'password_here');/define('DB_PASSWORD', 'nasp18');/" ./wordpress/wp-config.php

sudo rsync -avP wordpress/ /usr/share/nginx/html/
sudo mkdir /usr/share/nginx/html/wp-content/uploads
sudo chown -R admin:nginx /usr/share/nginx/html/*
