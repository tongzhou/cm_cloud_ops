#!/bin/bash - 
#===============================================================================
#
#          FILE: pxe_server_create.sh
# 
#         USAGE: ./pxe_server_create.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Assumes that virtual machine doesn't already exist.
#        AUTHOR: Thomas Lane
#  ORGANIZATION: 
#       CREATED: 11/07/16 16:22
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare script_dir=$(dirname $0)
#declare vms_folder="/home/tlane/vms/vboxvms/"
declare vm_name="cm_co_pxe_server" 
declare install_iso="/home/tliu/nasp_cm_cloud_ops/software/CentOS-7-x86_64-Minimal-1511_w_ks.iso"
declare vm_group="nasp_cm_co"
declare nat_network="nasp_cm_co"

#create vm in specified folder
vboxmanage createvm --name $vm_name --register

#Cludge to get the path of the directory where the vbox file is stored: creates file adjacent to vbox file
# vboxmanage showvminfo displays line with the path to the config file -> grep "Config file returns it
# the extended regex `(/[^/]+)+' matches everything that is a path i.e. / followed  by anthing not / 
#declare vbox_directory=$(dirname $(vboxmanage showvminfo ${vm_name}| grep "Config file" | grep -oE '(/[^/]+)+'))
declare vbox_directory=/home/tliu
#create virtual hard disk
declare hd_file=${vbox_directory}/${vm_name}.vdi
vboxmanage createhd --filename ${hd_file} --size 10000 -variant Standard

#add storage controllers for the optical and hard disks
vboxmanage storagectl $vm_name --name ide_ctrlr --add ide --bootable on
vboxmanage storagectl $vm_name --name sata_ctrlr --add sata --bootable on

#attach the installation iso this has an embemded kick start file and a custom boot option to invoke it
vboxmanage storageattach $vm_name --storagectl ide_ctrlr --port 0 --device 0 --type dvddrive --medium "${install_iso}"
#attach the virtualbox guest additions iso file - used to install guest additions (done in the kickstarter) file
vboxmanage storageattach $vm_name --storagectl ide_ctrlr --port 0 --device 1 --type dvddrive --medium "/usr/share/virtualbox/VBoxGuestAdditions.iso"

#attach the hard disk and specify that its an SSD 
vboxmanage storageattach $vm_name --storagectl sata_ctrlr --port 0 --device 0 --type hdd --medium ${hd_file} --nonrotational on

#configure the vm
vboxmanage modifyvm $vm_name\
    --ostype "RedHat_64"\
    --cpus 1\
    --hwvirtex on\
    --nestedpaging on\
    --largepages on\
    --firmware efi\
    --nic1 natnetwork\
    --nat-network1 "${nat_network}"\
    --nictype1 virtio\
    --cableconnected1 on\
    --audio none\
    --boot1 dvd\
    --boot2 disk\
    --boot3 none\
    --boot4 none\
    --natdnshostresolver1 on\
    --memory 1024 

#start the vm
vboxmanage startvm $vm_name --type gui

echo "finished"

# TODO: eject CD's from vm only once it has rebooted
# Following gets current state of vm
# vboxmanage showvminfo "aws_cm_pxe_server" | grep -o "State:\s\+\w\+" | grep -oE "\w+$"


