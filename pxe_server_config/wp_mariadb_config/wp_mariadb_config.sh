
#log to journal showing script start
systemd-cat -p "notice" -t wp_mariadb_config printf "%s" "wp_mariadb_config.sh start" 

#execute wp_mariadb_config.sql statements as the root mysql user, 
mysql -u root < /root/wp_mariadb_config.sql
#remember password for root hasn't been set yet

#Disable the wp_mariadb_config.service
systemctl disable wp_mariadb_config.service
#log to journal showing script end
systemd-cat -p "notice" -t wp_mariadb_config printf "%s" "wp_mariadb_config.sh end" 
