#!/bin/bash - 
#===============================================================================
#
#          FILE: vm_install.sh
# 
#         USAGE: ./vm_install.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 21/07/16 15:34
#      REVISION:  ---
#===============================================================================

set -o nounset  
                            # Treat unset variables as an error

declare network_name=nasp_cm_co
declare vm_name=wp_ansible
declare vms_folder=/home/tliu
declare size_in_mb=10000
declare ctrlr_name1=ide
declare ctrlr_name2=sata
declare ctrl_type1=ide
declare ctrl_type2=sata
declare memory_mb=1024
declare group_name=VM
declare port_num1=0
declare port_num2=1
declare device_num1=0
declare device_num2=1
declare zone=public

scp ./wp_ks.cfg  pxe:/usr/share/nginx/html/


vboxmanage createvm --name $vm_name --basefolder $vms_folder --register

vboxmanage createhd --filename $vms_folder/$vm_name.vdi  --size $size_in_mb --variant standard

vboxmanage storagectl $vm_name --name $ctrlr_name1 --add $ctrl_type1 --bootable on
vboxmanage storagectl $vm_name --name $ctrlr_name2 --add $ctrl_type2 --bootable on

vboxmanage storageattach $vm_name --storagectl $ctrlr_name1 --port $port_num1 --device $device_num1 --type dvddrive --medium "/home/tliu/ISOs/CentOS-7-x86_64-Minimal-1511.iso"
vboxmanage storageattach $vm_name --storagectl $ctrlr_name1 --port $port_num2 --device $device_num2 --type dvddrive --medium "/usr/share/virtualbox/VBoxGuestAdditions.iso"
vboxmanage storageattach $vm_name --storagectl $ctrlr_name2 --port $port_num1 --device $device_num1 --type hdd --medium ${vms_folder}/${vm_name}.vdi --nonrotational on

vboxmanage modifyvm $vm_name\
    --ostype "RedHat_64"\
    --cpus 1\
    --hwvirtex on\
    --nestedpaging on\
    --largepages on\
    --firmware BIOS\
    --nic1 natnetwork\
    --nictype1 "82543GC"\
    --nat-network1 "$network_name"\
    --cableconnected1 on\
    --macaddress1 020000000001\
    --audio none\
    --boot1 disk\
    --boot2 net\
    --boot3 dvd\
    --boot4 none\
    --memory "$memory_mb"

vboxmanage startvm $vm_name --type gui


